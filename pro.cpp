/***    TP3                             ***/
/*     auteurs Brochard, BARAGAA           */
/*     débuté le 20 novembre 2019        */
/*                                        */
/******************************************/

#include <iostream>   		// pour pouvoir utiliser cin, cout, endl
#include <cmath>   			// pour pouvoir utiliser sqrt
using namespace std;  	    // pour éviter de devoir écrire std::cin, std::cout, std::endl
#include "outilsmesure.hpp"    // les outils de mesure de temps

typedef long DATATYPE ;

typedef struct _datum {
    DATATYPE valeur ;
    _datum * suiv;
} data ;

typedef data * p_data ;

// la liste .............

typedef struct _datallst {
    int capa;
    int nbmono;
    p_data * monotonies;
} datalistes;



// Rôle : affiche un chaînage non vide (un chainage vide n’affichera rien)
void aff(p_data chain){
    while (chain != nullptr) {
        cout << (*chain).valeur << " ";
        chain = (*chain).suiv;
        
    }
    cout << endl;
}

// Rôle : créer un nouveau maillon avec la valeur passé en paramètres et l'insère avant le pointeur du maillon passé en paramètres 
p_data ajoutDevant(DATATYPE uneval, p_data chain){
    
    p_data test = new data();
    (*test).valeur = uneval;
    (*test).suiv = chain;
    chain = test;
    return chain;
    
}

// Rôle : créer un nouveau maillon avec la valeur passé en paramètres et l'insère à la fin du chaînage passé en paramètres
p_data ajoutDerriere(DATATYPE uneval, p_data chain){
    p_data deb = chain;
    p_data test = new data();
    (*test).valeur = uneval;
    (*test).suiv = nullptr;
    if(chain != nullptr){
        while((*chain).suiv != nullptr) {
            chain = (*chain).suiv;
        }
        (*chain).suiv = test;
    }
    else {
        deb = test;
    }
   
    return deb;
}

// Rôle :  passe à la valeur suivante de la chaîne, en la supprimant 
// Précondition : la chaîne contient une valeur suivante
// Postcondition : renvois une chaîne sans le premier maillon
p_data supprimerDevant(p_data chain){
    p_data temp = (*chain).suiv;
    delete(chain); 
    return temp;
}

// Rôle : demande des valeurs tant que la valeur "borne" n'a pas été saisie et les ajoutes en suite dans un nouveau chaînage.
p_data saisieBorne(DATATYPE sentinelle){

    p_data deb = nullptr;
    long valeur;
    do {
        cin >> valeur;
        if(sentinelle != valeur) {
            deb = ajoutDerriere(valeur, deb);
        }
    } 
    while(sentinelle != valeur);
    return deb;

}

// Rôle : saisie d'un nombre donné de valeurs dans une chaîne
// Précondition : nombre > 0 de valeurs
// Postcondition : renvois une chaîne
p_data saisieNombre(int nb){
    cout << "Saisir " << nb << " nombres" << endl;
    p_data deb = nullptr;
    long valeur;
    for(int i = 1; i <= nb ; i++) {
        cin >> valeur;
        
        deb = ajoutDerriere(valeur, deb);
        
    }
    return deb;


}

// Rôle : fusionne 2 chaînes rangées par ordre croissant et renvoie une chaîne rangée par ordre croissant
// Précondition : chaînes rangées par ordre croissant
// Postcondition  : chaîne rangée par ordre croissant
p_data fusion(p_data d1, p_data d2){
    p_data neuf = nullptr;
    p_data p = d1;
    p_data q = d2 ;
    while(p != nullptr || q != nullptr){
        if(p != nullptr && q != nullptr) {
            if ((*p).valeur <= (*q).valeur){
                neuf = ajoutDerriere((*p).valeur, neuf);
                p = (*p).suiv;
            }
            else {
                neuf = ajoutDerriere((*q).valeur, neuf);
                 q = (*q).suiv;
            }
        }
        else if(p == nullptr) {
            neuf = ajoutDerriere((*q).valeur, neuf);
            
             q = (*q).suiv;
        }
        else if(q == nullptr){
            neuf = ajoutDerriere((*p).valeur, neuf);
            p = (*p).suiv;
        }
        else {
            cout << "NON";
 
        }
        
    }

    return neuf;
}

// Rôle : fusionne 2 chaînes rangées par ordre croissant et renvoie une chaîne rangée par ordre croissant par récurrence
// Précondition : chaînes rangées par ordre croissant
// Postcondition  : chaîne rangée par ordre croissant
p_data fusionrec(p_data d1, p_data d2) {
    if(d1 == nullptr && d2 == nullptr) {
        return nullptr;
    }
    else if(d1 == nullptr){
        int test = (*d2).valeur;
        return ajoutDevant(test, supprimerDevant(d2));
    }
    else if(d2 == nullptr){
        int test = (*d1).valeur;
        return ajoutDevant(test, supprimerDevant(d1));
    }
    else if((*d1).valeur <= (*d2).valeur){
        int test = (*d1).valeur;
        return ajoutDevant(test, fusionrec(supprimerDevant(d1), d2));
    }
    else {
        int test = (*d2).valeur;
        return ajoutDevant(test, fusionrec(d1, supprimerDevant(d2)));
    }
}



// Rôle : affiche un chaînage non vide (un chainage vide n’affichera rien) par récurrence
void affrec(p_data chain){
    cout << (*chain).valeur << endl;
    if((*chain).suiv != nullptr) {
        affrec((*chain).suiv);
    }
}

// Rôle : affiche un chaînage non vide (un chainage vide n’affichera rien) à l'envers (du dernier chaînage jusqu'au premier)
void reversAff(p_data chain){
    int j = 0 ;
    p_data c = chain;
    while (c != nullptr) {
        j++;
        c = (*c).suiv;
        
    }
    DATATYPE list[j] = {} ;
    for (int i =1  ; i<=j;i++){
        list[i] = (*chain).valeur;
        chain = (*chain).suiv;
    }
    for (int i = j; i>=1 ; i--){
        cout << list[i] << endl;
    }
}

// Rôle : compter le nombre des monotonies croissantes dans un chaînage
// Précondition chaînage examiné non vide
// Postcondition : nombre des monotonies croissantes dans le chaînage en sortie.
int nbCroissances(p_data chain) {
    int compt = 1;
    for(p_data courant = chain; (*courant).suiv !=nullptr; courant = (*courant).suiv){
        if((*courant).valeur > (*(*courant).suiv).valeur){ 
            compt ++;
        }
    }
    return compt;      
}

// Rôle : extrait un chaînage croissant d’un autre chaînage en le supprimant de celui-ci.
// Préconditions:
//      le premier chaînage (parent) n'est pas vide  
//      le deuxième chaînage est vide 
// Postconditions:
//      les donnés ajoutés au deuxième chaînage sont supprimé du premier.
//      le deuxième chaînage est non vide et trié de manière croissant. 

void extraireCroissance(p_data & chain, p_data & mono) {

    mono = ajoutDerriere((*chain).valeur,mono);
    p_data tmp = mono; // un referrnce de debut de la chaine
    chain = supprimerDevant(chain);
  
   
    
    while (chain != nullptr &&(*mono).valeur <= ((*chain).valeur)) {
            
        mono = ajoutDerriere((*chain).valeur,mono);
        chain = supprimerDevant(chain);
        mono = (*mono).suiv;
            
        }
        
    mono = tmp; //recuperer le debut de la liste
    
    
}

// Rôle :définir une dataliste avec un nombre de monotonies
datalistes initT(int nb) {
    datalistes dat;
    dat.capa = nb;
    dat.nbmono = 0;
    dat.monotonies = new p_data[nb];
    return dat;
}

// Rôle : ajoute une monotonie dans un dataliste.
// Précondition :
//      nombre de monotonies < capa 
//      le chaînage ajouté n’est pas vide
void ajouterFin(p_data chain, datalistes & mono) {
    if(mono.nbmono < mono.capa){
        mono.monotonies[mono.nbmono] = chain;
	    mono.nbmono++;
    }
}

// Rôle : affiche une dataliste non vide (une dataliste vide n’affichera rien)
void affT(datalistes mono) {
    for (int i = 0; i < mono.nbmono; i++) {
        cout << "Mono " << i+1 << ":" << endl;
        aff(mono.monotonies[i]);
    }
}

// Rôle : suprime la dernière monotonie dans une dataliste.  
// Préconditions : le tableau de monotonies a au moins 2 monotonies non vides. 
// Postconditions : le nombre de monotonies est décrémenté de 1 et le dernier cas est nullptr.
p_data suppressionFin(datalistes & mono) {
    mono.monotonies[mono.nbmono] = nullptr;
	mono.nbmono--;
    return mono.monotonies[mono.nbmono];
}

// Rôle : vider une dataliste 
// Préconditions: dataliste non vide et ne manquant aucune information(ex:nbmono)
// PostCondition: toutes les monotonies sont assigné à nullptr
p_data suppressionTotale(datalistes & mono) {
    p_data deb = nullptr;
    int nb = mono.nbmono-1;
	for (int i = nb; i >= 0; i--) {
		deb = fusion(deb, mono.monotonies[i]);
        suppressionFin(mono);
	}

	return deb;
}

// Rôle : sépare un chaînage aux monotonies triés 
// Précondition: chaînage reçu non vide
// Postcondition: une dataliste de monotonies triés est renvoyé.
datalistes separation(p_data & chain) {
	datalistes dat;
	p_data mono = nullptr;
	int nb = nbCroissances(chain);
	dat = initT(nb);

	for(int i = 0; i < nb; i++){
		extraireCroissance(chain, mono);
		ajouterFin(mono, dat);
        mono = nullptr;
	}

	return dat;
}

// Rôle : trie les monotonies dans la première monotonie.
// Précondition: dataliste non vide de monotonies.
// Postcondition: une liste dont la première monotonie est la seule remplie avec les donnés des monotonies trié.
void trier(datalistes & tabmono){
	p_data  fin;
	int nb = tabmono.nbmono-1;
	
	for (int i = 0; i < nb; i++) {
		fin = suppressionFin(tabmono);
		tabmono.monotonies[0] = fusion(tabmono.monotonies[0], fin);
	}
}

// Rôle : trier un chainage en utilisant les autres fonctions/procédures.
// Précondition: dataliste non vide de monotonies.
// Postcondition: le chaînage inséré en paramètre est modifié vers un chaînage bien trié en utilisant la fusion multiple.
void trier(p_data & chain) {
	datalistes dat;
	dat = separation(chain);
    affT(dat);
	trier(dat);
	chain = suppressionFin(dat);

   // aff(chain);
}


int main() {
    // calcul temporel -------------------------------------------
    p_data d1 = new data();
    p_data d2 = new data();
    int nbChainages = 10000;
    for(int i = 0; i < nbChainages; i++) {
        d1 = ajoutDevant(i, d1);
        d2 = ajoutDevant(i, d2);
    }
    START;
    fusionrec(d1, d2);
    STOP;
    cout << "temps pour " << nbChainages << ": " << (long)floor(TEMPS) << endl;

    p_data pdata;
    pdata = new data();
    (*pdata).valeur = 450927186;


    pdata = ajoutDerriere(123, pdata);
    pdata = ajoutDerriere(124, pdata);
    pdata = ajoutDerriere(125, pdata);
    pdata = ajoutDerriere(16, pdata);
    pdata = ajoutDerriere(2, pdata);
    pdata = ajoutDerriere(7, pdata);

    //cout << "toute la chaine: " << endl;
    //aff(pdata);
    //affrec(pdata);
    //cout << "inverse" << endl;
    //reversAff(pdata);
    //cout << "entrez 6 + 4 nombres croissants" << endl;
    //p_data test = saisieNombre(6);
    //p_data test3 = saisieNombre(4);

    cout << "---------------Separation---------------" << endl;
    p_data test0 = saisieNombre(10);
    datalistes dat;
    dat = separation(test0);
    affT(dat);
    trier(dat);
    test0 = suppressionTotale(dat);
    aff(test0);
    cout << "---------------SuppressionTotale---------------" << endl;
    p_data test2 = saisieNombre(10);
    datalistes dat1;
    dat1 = separation(test2);
    p_data test3 = suppressionTotale(dat1);
    cout << "resultats" << endl;
    affT(dat1);
    aff(test3);
    cout << "---------------Tri---------------" << endl;
    p_data test1 = saisieNombre(10);
    trier(test1);
    cout << "resultat" << endl;
    aff(test1);
/*
    cout << "---------------fusion---------------" << endl;
    p_data test2 = fusion(test3, test);
    aff(test2);
    cout << "---------------fusionrec---------------" << endl;
    p_data testrec = fusionrec(test3, test);
    aff(testrec);

    datalistes dat = initT(2);
    ajouterFin(test, dat);
    ajouterFin(test3, dat);
    cout << "---------------supressionTotale---------------" << endl;
    p_data out = suppressionTotale(dat);
    cout << "---------------aff---------------" << endl;
    aff(out);
    cout << "---------------affT---------------" << endl;
    affT(dat);

    cout << "---------------nbCroissances---------------" << endl;
    p_data test4 = saisieNombre(6);
    cout << nbCroissances(test4)<<" " << "nb croissants" << endl;
    p_data mono = nullptr ;
    extraireCroissance(test4, mono);
    cout <<"mono \n";
    aff(mono) ;
    cout << "test4 \n"; 
    aff(test4);
*/
}